import scipy.stats as stats
import numpy as np

# Initialisieren der Parameter der Option
S0=42 # Initialer Preis der Aktie
K=40 # Ausübungspreis der Option
U=38 # Knock-Out-Schwelle; wenn der Aktienpreis unter diesen Wert fällt, wird die Option wertlos
T=2 # Laufzeit der Option in Jahren
r=0.02 # Risikoloser Zinssatz, den die Bank für eine sichere Anlage zahlt
sigma= 0.20 # Volatilität des Aktienpreises, ein Maß für die Unsicherheit in den zukünftigen Aktienpreisen
sim= 100000 # Anzahl der Monte-Carlo-Simulationen
tage= 500 # Anzahl der Tage, die in jeder Simulation simuliert werden

# Initialisierung des Simulationsarrays
# Die erste Spalte wird auf den initialen Aktienpreis gesetzt, der Rest wird zunächst auf Null gesetzt
array=np.zeros(shape=(sim,tage))
array[:,0]=S0

# Berechnung der täglichen Aktienpreisveränderungen mit der Black-Scholes-Formel
# und Anwendung auf das Simulationsarray
array[:,1:]= np.exp((r-sigma**2/2)*T/tage+sigma*np.sqrt(T/tage)*stats.norm.rvs(size=(sim, tage-1)))

# Berechnung des kumulierten Aktienpreises für jeden Tag
for i in range(1,tage):
    array[:,i]=array[:,i-1]*array[:,i]

# Bestimmung der Simulationen, in denen der Aktienpreis unter die Knock-Out-Schwelle fällt
x=np.min(array, axis=1)
knockoutZeilen= x<U

# Bestimmung der Auszahlungen am Ende der Laufzeit, falls die Option nicht ausgeknockt wurde
zv= array[:,-1]-K
ggfAusgezahlt= np.fmax(zv, 0)
ggfAusgezahlt[knockoutZeilen]=0

# Abzinsung der Auszahlungen auf den heutigen Wert
abgezinst=ggfAusgezahlt*np.exp(-r*T)

# Ausgabe des erwarteten Preises der Option unter Berücksichtigung des Knockouts
print("Preis mit Knockout (Simulation):", abgezinst.mean())

# Berechnung und Ausgabe des Preises der Option ohne Knockout nach dem Black-Scholes-Modell
d1=(np.log(S0/K)+(r+sigma**2/2)*T)/(sigma*np.sqrt(T))
d2=(np.log(S0/K)+(r-sigma**2/2)*T)/(sigma*np.sqrt(T))
preisCallOption=S0*stats.norm.cdf(d1)-K*np.exp(-r*T)*stats.norm.cdf(d2)
print("Preis ohne Knockout (Black Scholes):", preisCallOption)
